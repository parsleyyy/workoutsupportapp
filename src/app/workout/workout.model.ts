export interface Workout {
  name: string;
  userId?: string;
  exercises: [];
}
