import { Injectable } from '@angular/core';
import { 
  HttpClient,
  HttpHeaders,
  HttpParams,
  HttpEventType
} from '@angular/common/http';

import { map, tap } from 'rxjs/operators';

import calls from '../../helpers/calls';
import { Workout } from './workout.model';

@Injectable({
  providedIn: 'root'
})
export class WorkoutService {

  constructor(private http: HttpClient) { }

  getWorkouts() {
    return this.http
      .get<Workout>(
        calls.workouts,
        {
          headers: new HttpHeaders({ "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImNrMnJqaTZiYzAwMDAwZ3VxZHRtbTl6enkiLCJlbWFpbCI6Im1AdC5jb20iLCJpYXQiOjE1NzMzMDIyMDV9.RQ3bjWWqQL3fQWCtSy_ihOBmnYNJOsf7Uc_1U3Zu-qw"}),
          responseType: 'json',
        }
      )
      .pipe(map(res => {
        const workoutsData: Workout[] = [];

        for(const key in res) {
          if(res.hasOwnProperty(key)) {
            workoutsData.push({
              ...res[key],
              userId: key
            });
          }
        }
        console.log(workoutsData);
        
        return workoutsData;
      }
    ))
  }

}


