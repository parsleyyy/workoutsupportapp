import { 
  Component, 
  OnInit,
  EventEmitter,
  Output,
  Input
} from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Workout } from '../workout.model';
import { WorkoutService } from '../workout.service';

@Component({
  selector: 'app-workout-list',
  templateUrl: './workout-list.component.html',
  styleUrls: ['./workout-list.component.css'],
  providers: [WorkoutService],
})
export class WorkoutListComponent implements OnInit {
  @Output() workoutWasSelected = new EventEmitter<Workout>();

  fetchedWorkouts: Workout[] = [];
  isWorkoutsFetching = false;
  workoutsError = false;

  constructor(
    private http: HttpClient,
    private workoutService: WorkoutService
  ) { }

  ngOnInit() {
    this.onFetchWorkouts();
  }

  onFetchWorkouts() {
    this.isWorkoutsFetching = true;
    this.workoutService.getWorkouts()
      .subscribe(workouts => {
        this.isWorkoutsFetching = false;
        this.fetchedWorkouts = workouts;
      });
  }

  onExerciseSelected(workout: Workout) {
    this.workoutWasSelected.emit(workout);
  }

}
