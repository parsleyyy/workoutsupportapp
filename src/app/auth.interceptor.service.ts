import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpEvent, HttpHandler, HttpRequest, HttpEventType } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

@Injectable({providedIn: 'root'})
export class AuthrInterceptorService implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    console.log('request is on its way');
    console.log(req.url);

    const modifiedReq = req.clone({
      headers: req.headers.append('authentication', 'my auth'),
    });

    return next.handle(modifiedReq).pipe(
      tap(event => {
        console.log(event);

        if(event.type === HttpEventType.Response) {
          console.log('reaponse: ');
          console.log(event.body);
        }        
      }))
  }
}
