import { Component } from '@angular/core';
import { faCoffee } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  loadedFeature = 'startPage';

  onNavigate(feature: string) {
    this.loadedFeature = feature;
  }
}
