import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { ExerciseService } from '../exercises/exercises.service';

@Injectable({providedIn: 'root'})
export class DataStorageService {
  constructor(private http: HttpClient, private exerciseService: ExerciseService) {}

  storeExercises() {
    const exercises = this.exerciseService.fetchExercises();
    console.log(exercises)

    this.http.put(
      'https://fitapp-97b21.firebaseio.com/exercises.json',
      exercises
    )
    .subscribe(res => {
      console.log('tuteeej')
    });
  }

}
