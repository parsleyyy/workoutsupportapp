import { 
  Directive,
  OnInit,
  ElementRef,
  Renderer2,
  HostListener,
  HostBinding,
  Input
} from '@angular/core';

@Directive({
  selector: '[appBasicCard]'
})
export class BasicCardDirective implements OnInit{
  @Input() defaultColor: string = 'green';
  @Input() anotherColor: string = 'black';
  @HostBinding('style.color') color: string = 'transparent';

  constructor(private eleRef: ElementRef, private ren: Renderer2) { }

  ngOnInit() {
    //this.ren.setStyle(this.eleRef.nativeElement, 'color', 'blue');
  }

  @HostListener('mouseenter') mouseover(eventData: Event) {
    this.ren.setStyle(this.eleRef.nativeElement, 'color', 'blue');
  }

  @HostListener('mouseleave') mouseleave(eventData: Event) {
    this.ren.setStyle(this.eleRef.nativeElement, 'color', 'transparent');
  }

}
