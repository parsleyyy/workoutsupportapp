import { 
  Component, 
  OnInit,
  EventEmitter,
  Output,
  Input
} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

import calls from '../../../helpers/calls';
import { Exercise } from '../../exercises/exercises.model';
import { ExerciseService } from '../../exercises/exercises.service';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-exercise-list',
  templateUrl: './exercise-list.component.html',
  styleUrls: ['./exercise-list.component.css'],
  providers: [ExerciseService],
})
export class ExerciseListComponent implements OnInit {
  @Output() exerciseWasSelected = new EventEmitter<Exercise>();

  loadedExercises: Exercise[] = [];
  isExerciseFetching= false;
  error = false;
  images = [944, 1011, 984].map((n) => `https://picsum.photos/id/${n}/900/500`);

  constructor(private http: HttpClient, private exerciseService: ExerciseService) {}

  ngOnInit() {
    this.onFetchExercises();
  } 

  onFetchExercises() {
    this.isExerciseFetching = true;
    this.exerciseService.fetchExercises().subscribe(exercises => {
      this.isExerciseFetching = false;
      this.loadedExercises = exercises;
    });
  }; 

  onCreateExercise(postData: Exercise) {
    this.exerciseService.createAndStoreExercise(postData.name, postData.type );

    console.log(postData)
  }; 

  onExerciseSelected(exercise: Exercise) {
    this.exerciseWasSelected.emit(exercise);
  }

  onExerciseAdded(exercise: Exercise) {
    console.log('jestem tutej, halu halu')
    this.loadedExercises.push(exercise);
  }

  onClearExercises() {
    this.exerciseService.deleteExercises().subscribe(() => {
      this.loadedExercises = [];
    })
  };
  
}
