import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpEventType }  from '@angular/common/http';

import { Exercise } from '../exercises/exercises.model';

import calls from '../../helpers/calls';
import { map, tap } from 'rxjs/operators';

@Injectable({providedIn: 'root'})
export class ExerciseService {
  
  constructor(private http: HttpClient) {}

  createAndStoreExercise(name: string, type: string) {
    const postData: Exercise = { name: name, type: type };

    this.http
      .post<Exercise>(
        calls.exercises,
        postData,  
        {
          observe: 'response',
          headers: new HttpHeaders({ 
            "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImNrMnJqaTZiYzAwMDAwZ3VxZHRtbTl6enkiLCJlbWFpbCI6Im1AdC5jb20iLCJpYXQiOjE1NzMzMDIyMDV9.RQ3bjWWqQL3fQWCtSy_ihOBmnYNJOsf7Uc_1U3Zu-qw"
          }),
        }
      )
      .subscribe(response => {
        console.log(response.body);
      })
  }

  fetchExercises() {
    let customParams = new HttpParams();
    customParams = customParams.append('name', 'martwy cionk');

    return this.http
      .get<Exercise>(
        calls.exercises,
        {
          headers: new HttpHeaders({ 
            "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImNrMnJqaTZiYzAwMDAwZ3VxZHRtbTl6enkiLCJlbWFpbCI6Im1AdC5jb20iLCJpYXQiOjE1NzMzMDIyMDV9.RQ3bjWWqQL3fQWCtSy_ihOBmnYNJOsf7Uc_1U3Zu-qw"
          }),        
          responseType: 'json',
        }
      )
      .pipe(map(response  => {
        const exercisesData: Exercise[] = [];        

        for(const key in response) {
          if(response.hasOwnProperty(key)) {
            exercisesData.push({ ...response[key], userId: key });
          }
        }
        return exercisesData;
    }));
  }

  updateExercise(newExercise: Exercise) {
    console.log('updated name ' + newExercise.name);
  }

  deleteExercises() {
    return this.http
      .delete(calls.exercises, {
        observe: 'events',
        responseType: 'json'
      })
      .pipe(
        tap(event => {
          console.log(event);

          if(event.type === HttpEventType.Sent) {
            // ...
          } else if (event.type === HttpEventType.Response) {
            console.log(event.body);
          }
        })
      )
  }
}
