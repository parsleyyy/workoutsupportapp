export interface Exercise {
  name: string;
  type: string;
  id?: string;
  difficulty?: string;
  mobility?: string;
  picture?: string;
}
