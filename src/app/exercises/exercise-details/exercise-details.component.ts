import { 
  Component, 
  OnInit, 
  Input 
} from '@angular/core';

import { Exercise } from '../exercises.model';

@Component({
  selector: 'app-exercise-details',
  templateUrl: './exercise-details.component.html',
  styleUrls: ['./exercise-details.component.css']
})
export class ExerciseDetailsComponent implements OnInit {
  @Input() exercise: Exercise;

  constructor() { }

  ngOnInit() {
  }

}
