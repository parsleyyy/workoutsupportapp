import { 
  Component, 
  OnInit,
  EventEmitter,
} from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Exercise } from '../exercises.model';
import { ExerciseService } from '../exercises.service';

@Component({
  selector: 'app-form-add-exercise',
  templateUrl: './form-add-exercise.component.html',
  styleUrls: ['./form-add-exercise.component.css']
})
export class FormAddExerciseComponent implements OnInit {
  isExercisePosting= false;
  addExerciseError = false;

  constructor(private http: HttpClient, private exerciseService: ExerciseService) {}

  ngOnInit() {
  }

  onExerciseAdded(postData: Exercise) {
    this.exerciseService.createAndStoreExercise(postData.name, postData.type );

    console.log(postData)
  }; 


}
