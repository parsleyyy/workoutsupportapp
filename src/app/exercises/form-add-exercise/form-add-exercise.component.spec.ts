import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormAddExerciseComponent } from './form-add-exercise.component';

describe('FormAddExerciseComponent', () => {
  let component: FormAddExerciseComponent;
  let fixture: ComponentFixture<FormAddExerciseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormAddExerciseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormAddExerciseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
