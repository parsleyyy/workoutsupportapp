import { 
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  EventEmitter,
  Output
} from '@angular/core';

import { Exercise } from '../../exercises.model';

@Component({
  selector: 'app-add-exercise',
  templateUrl: './add-exercise.component.html',
  styleUrls: ['./add-exercise.component.css'],
})
export class AddExerciseComponent implements OnInit {
  @ViewChild('nameInput', { static: false }) nameInputRef: ElementRef;
  @ViewChild('typeInput', { static: false }) typeInputRef: ElementRef;
  // @ViewChild('difficultyInput', { static: false }) difficultyInputRef: ElementRef;
  // @ViewChild('bodyPartInput', { static: false }) bodyPartInputRef: ElementRef;
  @Output() exerciseAdded = new EventEmitter<Exercise>();
  
  constructor() { }

  ngOnInit() {
  }

  onAddItem() {
    const name = this.nameInputRef.nativeElement.value;
    const type = this.typeInputRef.nativeElement.value;
    
    console.log(name, type);
    
    this.exerciseAdded.emit(<Exercise>{name, type});
  }

}
