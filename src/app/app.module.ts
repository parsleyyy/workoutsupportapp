import { BrowserModule } from '@angular/platform-browser';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

// import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { 
  NgModule,
  CUSTOM_ELEMENTS_SCHEMA,
 } from '@angular/core';
import { 
  HttpClientModule, 
  HTTP_INTERCEPTORS
} from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { 
  RouterModule, 
  Routes 
} from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AuthrInterceptorService } from './auth.interceptor.service';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { ExercisesComponent } from './exercises/exercises.component';
import { ExerciseListComponent } from './exercises/exercise-list/exercise-list.component';
import { ExerciseItemComponent } from './exercises/exercise-list/exercise-item/exercise-item.component';
import { ExerciseDetailsComponent } from './exercises/exercise-details/exercise-details.component';
import { AddExerciseComponent } from './exercises/form-add-exercise/add-exercise/add-exercise.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { WorkoutComponent } from './workout/workout.component';
import { WorkoutListComponent } from './workout/workout-list/workout-list.component';
import { WorkoutItemComponent } from './workout/workout-item/workout-item.component';
import { WorkoutDetailsComponent } from './workout/workout-details/workout-details.component';
import { FormAddExerciseComponent } from './exercises/form-add-exercise/form-add-exercise.component';
import { BasicCardDirective } from './shared/directives/basic-card.directive';
import { SetterExampleDirective } from './shared/directives/setter-example.directive';
import { DropdownDirective } from './shared/directives/dropdown.directive';
import { LoginComponent } from './login/login.component';
import { IntroductionComponent } from './introduction/introduction.component';
import { DescriptionComponent } from './description/description.component';
import { StartPageComponent } from './start-page/start-page.component';

const appRoutes: Routes = [
  { path: 'create-plan', component: ExerciseListComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ExercisesComponent,
    ExerciseListComponent,
    ExerciseItemComponent,
    ExerciseDetailsComponent,
    AddExerciseComponent,
    WorkoutComponent,
    WorkoutListComponent,
    WorkoutItemComponent,
    WorkoutDetailsComponent,
    FormAddExerciseComponent,
    BasicCardDirective,
    SetterExampleDirective,
    DropdownDirective,
    LoginComponent,
    IntroductionComponent,
    DescriptionComponent,
    StartPageComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    RouterModule.forRoot(
      appRoutes,
    ),
    BrowserAnimationsModule,
    AngularFontAwesomeModule,
    FontAwesomeModule,
    NgbModule
  ],
  exports: [
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS, 
      useClass: AuthrInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule { }
