var calls:  { 
  exercises1: string,
  workouts: string,
  exercises: string,
  addWorkout: string,
  addExercise: string
} = {
  exercises1: '/api/exercises',
  workouts: 'https://hgkt-workout-api.herokuapp.com/api/workout',
  exercises: 'https://hgkt-workout-api.herokuapp.com/api/exercise',
  addWorkout: '',
  addExercise: 'https://hgkt-workout-api.herokuapp.com/api/exercise'
};

export default calls;
